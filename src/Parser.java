import trufflebf.nodes.*;


import java.util.*;
import java.io.*;

public class Parser {

    public static MainNode parse(String input)
    {
        return new MainNode(parse(new StringReader(input)));

    }

    public static BFNode parse(Reader input)
    {
        List<BFNode> bf = new ArrayList<BFNode>();
        while (true)
        {
            int v;
            try {
                v = input.read();
            }
            catch (IOException e)
            {
                v = -1;
            }
            if (v == '+')
                bf.add(new IncInNode());
            else if (v == '-')
                bf.add(new DecInNode());
            else if (v == '<')
                bf.add(new DecNode());
            else if (v == '>')
                bf.add(new IncNode());
            else if (v == '[')
                bf.add(parse(input));
            else if (v == ']')
                return new LoopNode(new BlockNode(bf.toArray(new BFNode[bf.size()])));
            else if (v == '.')
                bf.add(new PrintNode());
            else if (v == -1)
                break;
        }
        return new BlockNode(bf.toArray(new BFNode[bf.size()]));
    }
}
