package trufflebf.nodes;
import com.oracle.truffle.api.frame.VirtualFrame;
import com.oracle.truffle.api.nodes.*;

public class MainNode extends RootNode
{
    @Child private final BFNode _entry;

    public MainNode(BFNode entry)
    {
        this._entry = adoptChild(entry);
    }

    @Override
    public Object execute(VirtualFrame vf) {
        byte[] data = new byte[32000];
        _entry.execute(vf, data, 0);
        return data;
    }

}