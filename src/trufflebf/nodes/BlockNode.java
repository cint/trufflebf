package trufflebf.nodes;
import com.oracle.truffle.api.frame.VirtualFrame;
import com.oracle.truffle.api.nodes.*;

public class BlockNode extends BFNode{

    @Children private final BFNode[] _children;

    public BlockNode(BFNode[] children)
    {
        this._children = adoptChildren(children);
    }

    @Override
    @ExplodeLoop
    public int execute(VirtualFrame frame, byte[] data, int idx) {
        int idxout = idx;
        for (BFNode node : _children)
            idxout = node.execute(frame, data, idxout);

        return idxout;

    }
}
