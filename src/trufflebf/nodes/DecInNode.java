package trufflebf.nodes;
import com.oracle.truffle.api.frame.VirtualFrame;
import com.oracle.truffle.api.nodes.*;

public class DecInNode extends BFNode
{
    @Override
    public int execute(VirtualFrame frame, byte[] data, int idx)
    {
        data[idx] -= 1;
        return idx;
    }

}