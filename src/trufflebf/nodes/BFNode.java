/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package trufflebf.nodes;
import com.oracle.truffle.api.frame.VirtualFrame;
import com.oracle.truffle.api.nodes.*;

/**
 *
 * @author tim
 */

public abstract class BFNode extends Node{
    public abstract int execute(VirtualFrame frame, byte[] data, int idx);
}

