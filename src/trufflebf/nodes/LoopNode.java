package trufflebf.nodes;
import com.oracle.truffle.api.frame.VirtualFrame;
import com.oracle.truffle.api.nodes.*;


public class LoopNode extends BFNode
{
    @Child private final BFNode _child;
    public LoopNode(BFNode child)
    {
        this._child = adoptChild(child);

    }

    @Override
    //@ExplodeLoop
    public int execute(VirtualFrame frame, byte[] data, int idx) {
        int idxout = idx;
        while (true) {
            idxout = _child.execute(frame, data, idxout);
            if (data[idxout] == 0)
                return idxout;
        }
    }
}