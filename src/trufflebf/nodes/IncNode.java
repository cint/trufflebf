package trufflebf.nodes;
import com.oracle.truffle.api.frame.VirtualFrame;
import com.oracle.truffle.api.nodes.*;

public class IncNode extends BFNode
{
    @Override
    public int execute(VirtualFrame frame, byte[] data, int idx) {
        return idx + 1;
    }
}