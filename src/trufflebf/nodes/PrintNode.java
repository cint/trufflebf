package trufflebf.nodes;
import com.oracle.truffle.api.frame.VirtualFrame;
import com.oracle.truffle.api.nodes.*;

public class PrintNode extends BFNode {


    @Override
    public int execute(VirtualFrame frame, byte[] data, int idx) {
        System.out.write(data[idx]);
        return idx;
    }
}
