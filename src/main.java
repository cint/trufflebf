
import com.oracle.truffle.api.*;
import com.oracle.truffle.api.frame.*;
import trufflebf.nodes.*;
import com.oracle.truffle.api.nodes.*;


public class main {
    public static void main(String[] args)
    {

        RootNode p = Parser.parse("++++++++++[>+++++++>++++++++++>+++>+<<<<-]>++.>+.+++++++..+++.>++.<<+++++++++++++++.>.+++.------.--------.>+.>.");
        CallTarget target = Truffle.getRuntime().createCallTarget(p, new FrameDescriptor());
        Object result = null;
        for (int x = 0; x < 10000; x ++)
        {

            long start = System.nanoTime();
            result = target.call(null, null);
            long end = System.nanoTime();

            System.out.printf("== iteration %d: %.3f ms\n", (x + 1), (end - start) / 1000000.0);
        }
        System.console().printf("foo", result);

    }
}
